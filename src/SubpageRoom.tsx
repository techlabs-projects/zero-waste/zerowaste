import React from 'react';
import { Link } from "react-router-dom";
//CSS File
import './App.css'
//Components
import NavbarSidebar from './Components/NavbarSidebar';
import TextComponent, { possibleClassNames_TextComponent } from './Components/TextComponent';
import Footer from './Components/Footer'
import ImageWithTextHovering from './Components/ImageWithTextHovering';
import { Line, possibleClassNames } from "./Components/Line";

//Images
import bathroomImage from './Images/house-bathroom.png';
import kitchenImage from './Images/house-kitchen.png';
import storageImage from './Images/house-storage.png';
import bedroomImage from './Images/house-bedroom.png';
//Text
import TextJSON from './Texts/text.json'


export enum possibleRooms {
    bathroom = "Page_Bathroom",
    bedroom = "Page_Bedroom",
    kitchen = "Page_Kitchen",
    storage = "Page_Storage",
}


interface ISubpageRoom_Properties {
    roomName: possibleRooms;
    roomImageSource: string;
    roomImageAlt: string;
    carouselComponentAndText?: JSX.Element;
}

export default class SubpageRoom extends React.Component<ISubpageRoom_Properties, {}>{
    render() {

        let room_text: any = TextJSON[this.props.roomName];
        let room_name: string = room_text["roomName"];
        let headline: string = room_text["headline_top"];
        let subtext_under_headline: string = room_text["subtext_top"];
        let headline_on_products : string = room_text["headline_on_products"];
        let text_on_products : string = room_text["text_on_products"];
        let headline_goToOtherRooms : string = room_text["headline_goToOtherRooms"];
        let otherRoomName1 : string, otherRoomName2 : string, otherRoomName3 : string;
        let roomImage1, roomImage2, roomImage3;
        
        //For showing the not present rooms in the ImageWithTextHovering Elements
        switch (room_name){
            case TextJSON.Page_Bathroom.roomName:
                otherRoomName1   = TextJSON.Page_Storage.roomName;
                otherRoomName2   = TextJSON.Page_Kitchen.roomName;
                otherRoomName3   = TextJSON.Page_Bedroom.roomName;
                roomImage1 = storageImage;
                roomImage2 = kitchenImage;
                roomImage3 = bedroomImage;
                break;
            case TextJSON.Page_Bedroom.roomName:
                otherRoomName1 = TextJSON.Page_Bathroom.roomName;
                otherRoomName2 = TextJSON.Page_Storage.roomName;
                otherRoomName3  = TextJSON.Page_Kitchen.roomName;
                roomImage1 = bathroomImage;
                roomImage2 = storageImage;
                roomImage3 = kitchenImage;
                break;
            case TextJSON.Page_Kitchen.roomName:
                otherRoomName1 = TextJSON.Page_Bathroom.roomName;
                otherRoomName2 = TextJSON.Page_Bedroom.roomName;
                otherRoomName3  = TextJSON.Page_Storage.roomName;
                    roomImage1 = bathroomImage;
                    roomImage2 = bedroomImage;
                    roomImage3 = storageImage;
                    break;
            case TextJSON.Page_Storage.roomName:
                otherRoomName1 = TextJSON.Page_Kitchen.roomName;
                otherRoomName2 = TextJSON.Page_Bedroom.roomName;
                otherRoomName3  = TextJSON.Page_Bathroom.roomName;
                roomImage1 = kitchenImage;
                roomImage2 = bedroomImage;
                roomImage3 = bathroomImage;
                break;
            default:
                otherRoomName1 = TextJSON.Page_Bathroom.roomName;
                otherRoomName2 = TextJSON.Page_Storage.roomName;
                otherRoomName3  = TextJSON.Page_Kitchen.roomName;
                roomImage1 = bathroomImage;
                roomImage2 = storageImage;
                roomImage3 = kitchenImage;
                break;
        }
        return (
            <div className="App">
                <NavbarSidebar NavbarText="ZeroWasteJourney" />
                <div className="Room-image-container">
                    <img className="Room-image" src={`.${this.props.roomImageSource}`} alt={this.props.roomImageAlt} />
                </div>
                <div className="Text-centered">
                    <Line class={possibleClassNames.h75} />
                    <TextComponent class={possibleClassNames_TextComponent.heading} text={headline} ></TextComponent>
                    <TextComponent class={possibleClassNames_TextComponent.paragraph} text={subtext_under_headline}></TextComponent>
                    <Line class={possibleClassNames.h50}/>
                </div>
                <div className= "Text-centered">
                    <TextComponent class={possibleClassNames_TextComponent.heading} text={headline_on_products}></TextComponent>
                    <TextComponent class = {possibleClassNames_TextComponent.paragraph} 
                    text={text_on_products}></TextComponent>
                    <Line class={possibleClassNames.h150} />
                </div>
                {this.props.carouselComponentAndText}
                <div className= "Text-centered">
                    <TextComponent class={possibleClassNames_TextComponent.heading} text={headline_goToOtherRooms}></TextComponent>
                </div>
                <div className="set-of-images-with-text-hovering">
                    <Link to={"/"+otherRoomName1}>
                        <ImageWithTextHovering imageSource={roomImage1} imageAlt={"Small Image of" + otherRoomName1 + "to navigate to the room page on click."} title="go to" text={otherRoomName1} />
                    </Link>
                    <Link to={"/"+otherRoomName2}>
                        <ImageWithTextHovering imageSource={roomImage2} imageAlt={"Small Image of" + otherRoomName2 + "to navigate to the room page on click."} title="go to" text={otherRoomName2} />
                    </Link>
                    <Link to={"/"+otherRoomName3}>
                            <ImageWithTextHovering imageSource={roomImage3} imageAlt={"Small Image of" + otherRoomName3 + "to navigate to the room page on click."} title="go to" text={otherRoomName3} />    
                    </Link>
                </div>
                <Line class={possibleClassNames.h175} />
                <Footer />
            </div>
        )
    }
}
