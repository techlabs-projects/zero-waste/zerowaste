import React from 'react';
//Css files
import './App.css';
//Components
import TextComponent, { possibleClassNames_TextComponent } from './Components/TextComponent';
import ImageAndText from './Components/ImageAndText';
import Footer from './Components/Footer'
import { Line, possibleClassNames } from "./Components/Line";
import SetOfPacMan from './Components/SetOfPacMan';
import NavbarSidebar from './Components/NavbarSidebar';
import ImageWithTextHovering from './Components/ImageWithTextHovering';
import HouseSvg from './Components/HouseSvg';
//Images
import refuse from './Images/principle-refuse.png';
import share from './Images/principle-share.png';
import repair from './Images/principle-repair.png';
import simplify from './Images/principle-simplify.png';
import recycle from './Images/principle-recycle.png';
import community from './Images/community-image.png';
//Text
import Text from './Texts/text.json'


export default class App extends React.Component {

  render() {
    return (
      <div className="App">
        <NavbarSidebar NavbarText="ZeroWasteJourney" />
        <Line class={possibleClassNames.h50} />
        <div className="Text-centered">
          <TextComponent text={Text.Page_Home.headline_top} class={possibleClassNames_TextComponent.heading} />
          <TextComponent text={Text.Page_Home.subtext_top} class={possibleClassNames_TextComponent.paragraph} />
          <Line class={possibleClassNames.h50} />
        </div>
        <div className="svgHouse-container">
          <HouseSvg />
        </div>
        <Line class={possibleClassNames.h125} />
        <div className="Text-centered">
          <TextComponent text={Text.Page_Home.headline_SetOfPacmans} class={possibleClassNames_TextComponent.heading} />
          <TextComponent text={Text.Page_Home.subtext_SetOfPacmans} class={possibleClassNames_TextComponent.paragraph} />
        </div>
        <SetOfPacMan />
        <Line class={possibleClassNames.h125} />
        <div className="Text-centered">
        <TextComponent text={Text.Page_Home.headline_basicrules} class={possibleClassNames_TextComponent.heading} />
        <TextComponent text={Text.Page_Home.subtext_basicrules} class={possibleClassNames_TextComponent.paragraph} />
        </div>
        <div className="set-of-images-with-text-hovering">
          <ImageWithTextHovering imageSource={refuse} imageAlt="Refuse Image" title="#1" text="REFUSE" />
          <ImageWithTextHovering imageSource={repair} imageAlt="Repair Image" title="#2" text="REUSE & REPAIR" />
          <ImageWithTextHovering imageSource={recycle} imageAlt="Recycle Image" title="#3" text="RECYCLE" />
          <ImageWithTextHovering imageSource={share} imageAlt="Rent & Share Image" title="#4" text="RENT & SHARE" />
          <ImageWithTextHovering imageSource={simplify} imageAlt="Simplify Image" title="#5" text="SIMPLIFY" />
        </div>
        <Line class={possibleClassNames.h125} />
        <ImageAndText imageSource={community} imageAlt={Text.Page_Home.imageAlt_community} title={Text.Page_Home.headline_imageAndText_community} text={Text.Page_Home.subtext_imageAndText_community} buttonText="Learn more"></ImageAndText>
        <Line class={possibleClassNames.h100} />
        <Footer />
      </div>
    );
  }
}
