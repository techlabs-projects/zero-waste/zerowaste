import React, { useEffect } from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
//For Routing
import { HashRouter as Router, Route, Switch } from "react-router-dom";
import SubpageRoom, { possibleRooms } from './SubpageRoom';
import SetOfPacMan from './Components/SetOfPacMan';
import CarouselBathroom from './Components/CarouselBathroom';
import { useLocation } from 'react-router-dom';
import CarouselBedroom from './Components/CarouselBedroom';
//SubPages images
import bathroomImage from './Images/subpage-bathroom.jpg';
import kitchenImage from './Images/subpage-kitchen.jpg';
import bedroomImage from './Images/subpage-bedroom.png';
import storageImage from './Images/subpage-storage.png';

export default function ScrollToTop() {
  const { pathname } = useLocation();

  useEffect(() => {
    window.scrollTo(0, 0);
  }, [pathname]);

  return null;
}

const BathroomPage = () => {
  return (
    <SubpageRoom
      roomName={possibleRooms.bathroom}
      roomImageSource={bathroomImage}
      roomImageAlt="A bathroom with a tub and a palm."
      carouselComponentAndText={<CarouselBathroom />} />
  )
}

const BedroomPage = () => {
  return (
    <SubpageRoom 
    roomName={possibleRooms.bedroom} 
    roomImageSource={bedroomImage} 
    roomImageAlt="A bed and a nightstand."
    carouselComponentAndText={<CarouselBedroom />} />
  )
}

const KitchenPage = () => {
  return (
    <SubpageRoom 
    roomName={possibleRooms.kitchen} 
    roomImageSource={kitchenImage} 
    roomImageAlt="A clean kitchen with plants and jars with unpackaged groceries " />
  )
}

const StoragePage = () => {
  return (
    <SubpageRoom 
    roomName={possibleRooms.storage} 
    roomImageSource={storageImage} 
    roomImageAlt="A shelf with a lot of different plants and some random things surrounding it." />
  )
}

class Routing extends React.Component {
  render() {


    return (
      <Router>
        <ScrollToTop />
        <Switch>
          <Route exact path="/" component={App} />
          <Route exact path="/Bathroom" render={BathroomPage} />
          <Route exact path="/Bedroom" render={BedroomPage} />
          <Route exact path="/Kitchen" render={KitchenPage} />
          <Route exact path="/Storage" render={StoragePage} />
          <Route exact path="/pacman" component={SetOfPacMan} />
        </Switch>
      </Router>

    )

  }
}

ReactDOM.render(
  <React.StrictMode>
    <Routing />
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
