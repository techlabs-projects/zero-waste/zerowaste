import React, { useEffect } from 'react';
import ReactDOM from 'react-dom';
import './Styles/Modal.css';

export interface ModalProps {
  isShown: boolean;
  closeModal: () => void;
  headerText: JSX.Element | HTMLElement | string;
  modalContent: JSX.Element | HTMLElement | string;
  imageSource: string;
  imageAlt: string
}

export default function Modal(props: ModalProps) {

  useEffect(() => {
    props.isShown ?
      (document.body.style.overflow = "hidden") : (document.body.style.overflow = "unset");
  }, [props.isShown]);

  const modal = (
    <div className="modal-backdrop">
      <div className="modal-wrapper">
        <div className="modal">
          <div className="modal-header">
            <h2 className="modal-title">{props.headerText}</h2>
            <button className="close-modal-icon" onClick={props.closeModal}>X</button>
          </div>
          <div className="modal-body">
            <img src={`.${props.imageSource}`} alt={props.imageAlt} />
            <p className="modal-content">{props.modalContent}</p>
          </div>
          <div className="modal-footer">
            <button className="close-modal-btn" onClick={props.closeModal}>Close</button>
          </div>
        </div>
      </div>
    </div>
  );

  return props.isShown ? ReactDOM.createPortal(modal, document.body) : null;
};