import React from "react";
import "./Styles/PacMan.css";

interface PacManProps {
  imageSource: string;
  imageAlt: string;
  text: string;
  handleClick?: () => void;  //Optional to make the use of the component more flexible
}

export default class PacMan extends React.Component<PacManProps, any> {

  render() {
    return (
      <div className="pacmanImgAndText" onClick={this.props.handleClick}>
        <div className="pacmanImg">
          <img src={`.${this.props.imageSource}`} alt={this.props.imageAlt} />
        </div>
        <div className="pacmanText">
          <p>{this.props.text}</p>
        </div>
      </div>
    );
  }
}
