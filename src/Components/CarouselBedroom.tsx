import React from "react";
//@ts-ignore
import Slider from "react-slick";

//Components
import TextComponent, { possibleClassNames_TextComponent } from './TextComponent';
import { Line, possibleClassNames } from "./Line";
//Images
import armedangelsLogo from '../Images/logo-armedangels.png';
import avocadostoreLogo from '../Images/logo-avocadostore.png';
import ebaykleinanzeigenLogo from '../Images/logo-ebaykleinanzeigen.png';
import fairmondoLogo from '../Images/logo-fairmondo.png';
import secondlifefashionLogo from '../Images/logo-secondlifefashion.png';
import schlafgutLogo from '../Images/logo-schlafgut.png';
import rightChevron from '../Images/chevron-right.svg';
import leftChevron from '../Images/chevron-left.svg';
//CSS file
import './Styles/Carousel.css';
//Text
import TextJSON from '../Texts/text.json'

export default class CarouselBathroom extends React.Component {
  carouselData = {
    items: [
      {
        id: 1,
        picture: <img src={"." +armedangelsLogo} />,
        link: "https://www.armedangels.com/de",
      },
      {
        id: 2,
        picture: <img src={"." +avocadostoreLogo} />,
        link: "https://www.avocadostore.de/",

      },
      {
        id: 3,
        picture: <img src={"." +ebaykleinanzeigenLogo} />,
        link: "https://www.ebay-kleinanzeigen.de/",
      },
      {
        id: 4,
        picture: <img src={"." +fairmondoLogo} />,
        link: "https://www.fairmondo.de/",
      },
      {
        id: 5,
        picture: <img src={"." +secondlifefashionLogo} />,
        link: "https://secondlifefashion.de/collections/bekleidung",
      },
      {
        id: 6,
        picture: <img src={"." +schlafgutLogo} />,
        link: "https://www.schlafgut.com/",
      },
    ],
  }

  render() {

    const { items } = this.carouselData;

    const settings = {
      arrows: true,
      dots: true,
      infinite: true,
      speed: 500,
      slidesToShow: 5,
      slidesToScroll: 3,
      responsive: [
        {
          breakpoint: 1520,
          settings: {
            slidesToShow: 4,
            slidesToScroll: 3,
            infinite: true,
            dots: true
          }
        },
        {
          breakpoint: 1250,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 3,
            infinite: true,
            dots: true
          }
        },
        {
          breakpoint: 992,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 2,
            infinite: true,
            dots: true
          }
        },
        {
          breakpoint: 600,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
            initialSlide: 1,
            infinite: true,
            dots: true
          }
        },
      ],
      nextArrow: (
        <div>
          <div className="next-slick-arrow">
            <img src={"." +rightChevron} />
          </div>
        </div >
      ),
      prevArrow: (
        <div>
          <div className="prev-slick-arrow">
            <img src={"." +leftChevron} />
          </div>
        </div>
      ),
    };
    return (
      <div>
        {/* Text presenting the carousel */}
        <div className="Text-centered">
          <TextComponent class={possibleClassNames_TextComponent.heading} text={TextJSON.Page_Bedroom.headline_brands} />
          <TextComponent class={possibleClassNames_TextComponent.paragraph}
            text={TextJSON.Page_Bedroom.text_brands}></TextComponent>
        </div>
        {/* Carousel */}
        <Slider {...settings}>
          {
            items.map(item =>
              <div className="slider-item" key={item.id}>
                <a href={item.link} target="_blank" rel="noopener noreferrer">{item.picture}</a>
              </div>)
          }
        </Slider >
        <div className="Text-centered">
        <TextComponent class={possibleClassNames_TextComponent.paragraph}
            text={TextJSON.Page_Bedroom.text_under_caroussel}></TextComponent>
          </div>
        {/* Line Component */}
        <Line class={possibleClassNames.h100}/>
      </div>
    );
  }
}