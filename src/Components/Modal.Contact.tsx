import React from 'react';
//Hooks
import { useModal } from '../Hooks/useModal';
//Components
import Modal from './Modal';
import TextComponent, { possibleClassNames_TextComponent } from './TextComponent';
//Images
import contact_picture from '../Images/modal-contact.png';
//Styles
import './Styles/TextComponent.css';
//Text
import Text from '../Texts/text.json';

export default function ModalContact() {
    const { isShown: isShownContact, toggle: toggleContact } = useModal();
    return (
        <div>
            <svg onClick={toggleContact}
                xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-envelope-open" viewBox="0 0 16 16">
                <path d="M8.47 1.318a1 1 0 0 0-.94 0l-6 3.2A1 1 0 0 0 1 5.4v.818l5.724 3.465L8 8.917l1.276.766L15 6.218V5.4a1 1 0 0 0-.53-.882l-6-3.2zM15 7.388l-4.754 2.877L15 13.117v-5.73zm-.035 6.874L8 10.083l-6.965 4.18A1 1 0 0 0 2 15h12a1 1 0 0 0 .965-.738zM1 13.117l4.754-2.852L1 7.387v5.73zM7.059.435a2 2 0 0 1 1.882 0l6 3.2A2 2 0 0 1 16 5.4V14a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V5.4a2 2 0 0 1 1.059-1.765l6-3.2z" />

            </svg>
            <Modal
                headerText={<TextComponent text={Text.Modal_Contact.modalContact_heading} class={possibleClassNames_TextComponent.heading} />}
                imageSource={contact_picture}
                imageAlt="Contact Picture"
                isShown={isShownContact}
                closeModal={toggleContact}
                modalContent=
                {
                    <div className="modal-content" >
                        <TextComponent text={Text.Modal_Contact.modalContact_content} class={possibleClassNames_TextComponent.paragraph} />
                    </div >}
            />
        </div>
    )
}

