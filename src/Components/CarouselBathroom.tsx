import React from "react";
//@ts-ignore
import Slider from "react-slick";

//Components
import TextComponent, { possibleClassNames_TextComponent } from './TextComponent';
import { Line, possibleClassNames } from "./Line";
//Images
import denttabsLogo from '../Images/logo-denttabs.png';
import hausedwolfLogo from '../Images/logo-hausedwolf.png';
import omakaLogo from '../Images/logo-omaka.png';
import reinlandLogo from '../Images/logo-reinland.png';
import muehleLogo from '../Images/logo-muehle.png';
import woodbergLogo from '../Images/logo-woodberg.png';
import taynieLogo from '../Images/logo-taynie.webp';
import benannaLogo from '../Images/logo-benanna.png';
import lastobjectLogo from '../Images/logo-lastobject.png';
import rightChevron from '../Images/chevron-right.svg';
import leftChevron from '../Images/chevron-left.svg';
//CSS file
import './Styles/Carousel.css';
//Text
import TextJSON from '../Texts/text.json'

export default class CarouselBathroom extends React.Component {
  carouselData = {
    items: [
      {
        id: 1,
        picture: <img src={`.${denttabsLogo}`} />,
        link: "https://denttabs.de/",

      },
      {
        id: 2,
        picture: <img src={"."+omakaLogo} />,
        link: "https://www.omaka.de/",
      },
      {
        id: 3,
        picture: <img src={"." +hausedwolfLogo} />,
        link: "https://www.hausedwolf.com/",
      },
      {
        id: 4,
        picture: <img src={"." +reinlandLogo} />,
        link: "https://reinland-seifen.de/",
      },
      {
        id: 5,
        picture: <img src={"." +woodbergLogo} />,
        link: "https://www.woodberg.de/",
      },
      {
        id: 6,
        picture: <img src={"." +benannaLogo} />,
        link: "https://ben-anna.com/",
      },
      {
        id: 7,
        picture: <img src={"." +muehleLogo} />,
        link: "https://www.muehle-shaving.com/",
      },
      {
        id: 8,
        picture: <img src={"." +taynieLogo} />,
        link: "https://www.taynie.de/",
      },
      {
        id: 9,
        picture: <img src={"." +lastobjectLogo} />,
        link: "https://lastobject.com/",
      },
    ],
  }

  render() {

    const { items } = this.carouselData;

    const settings = {
      arrows: true,
      dots: true,
      infinite: true,
      speed: 500,
      slidesToShow: 5,
      slidesToScroll: 3,
      responsive: [
        {
          breakpoint: 1520,
          settings: {
            slidesToShow: 4,
            slidesToScroll: 3,
            infinite: true,
            dots: true
          }
        },
        {
          breakpoint: 1250,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 3,
            infinite: true,
            dots: true
          }
        },
        {
          breakpoint: 992,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 2,
            infinite: true,
            dots: true
          }
        },
        {
          breakpoint: 600,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
            initialSlide: 1,
            infinite: true,
            dots: true
          }
        },
      ],
      nextArrow: (
        <div>
          <div className="next-slick-arrow">
            <img src={"." +rightChevron} />
          </div>
        </div >
      ),
      prevArrow: (
        <div>
          <div className="prev-slick-arrow">
            <img src={"." +leftChevron} />
          </div>
        </div>
      ),
    };
    return (
      <div>
        {/* Text presenting the carousel */}
        <div className="Text-centered">
          <TextComponent class={possibleClassNames_TextComponent.heading} text={TextJSON.Page_Bathroom.headline_brands} />
          <TextComponent class={possibleClassNames_TextComponent.paragraph}
            text={TextJSON.Page_Bathroom.text_brands}></TextComponent>
        </div>
        {/* Carousel */}
        <Slider {...settings}>
          {
            items.map(item =>
              <div className="slider-item" key={item.id}>
                <a href={item.link} target="_blank" rel="noopener noreferrer">{item.picture}</a>
              </div>)
          }
        </Slider >
        <div className="Text-centered">
        <TextComponent class={possibleClassNames_TextComponent.paragraph}
            text={TextJSON.Page_Bathroom.text_under_caroussel}></TextComponent>
          </div>
        {/* Line Component */}
        <Line class={possibleClassNames.h150}/>
      </div>
    );
  }
}
