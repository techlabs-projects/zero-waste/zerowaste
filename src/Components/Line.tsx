import React from "react";
import './Styles/Line.css';

export enum possibleClassNames {
  h50 = "line h50",
  h75 = "line h75",
  h100 = "line h100",
  h125 = "line h125",
  h150 = "line h150",
  h175 = "line h175",
  h200 = "line h200",
}

interface lineComponentsProperties {
  class: possibleClassNames;
}

export class Line extends React.Component<lineComponentsProperties, {}> {
  render() {
    return (
      <div className="line-container">
        <div className={this.props.class}></div>
      </div>
    );
  }
}
