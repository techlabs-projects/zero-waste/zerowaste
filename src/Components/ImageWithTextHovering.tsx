import React from "react";
import "./Styles/ImageWithTextHovering.css";

interface ImageWithTextHoveringProps {
  imageSource: string;
  imageAlt: string;
  title: string;
  text: string;
}

export default class ImageWithTextHovering extends React.Component<ImageWithTextHoveringProps,{}> {
  render() {
    return (
        <div className="image-and-text-box">
          <img
              className="image-box"
              src={`.${this.props.imageSource}`}
              alt={this.props.imageAlt}
          />
          <div className="text-box">
            <h4>{this.props.title}</h4>
            <p>{this.props.text}</p>
          </div>
        </div>
    );
  }
}
