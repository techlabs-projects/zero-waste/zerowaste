import React from "react";
//Css files
import './Styles/SetOfPacMan.css';
import './Styles/TextComponent.css';
//Components
import PacMan from './PacMan';
import Modal from './Modal'
import TextComponent, { possibleClassNames_TextComponent } from './TextComponent';
// Hooks
import { useModal } from '../Hooks/useModal';
//Images
import pacman from '../Images/pacman.svg';
import modalBulkShopPicture from '../Images/modal-bulk-shops.png';
import modalFoodSavingPicture from '../Images/modal-food-saving.png';
import modalToGoPackingPicture from '../Images/modal-to-go-packing.png';
import modalThriftShopPicture from '../Images/modal-thrift-shops.png';
//Text
import Text from '../Texts/text.json';

export default function SetOfPacMan() {
  const { isShown: isShownBulkShops, toggle: toggleBulkShops } = useModal();
  const { isShown: isShownFoodSaving, toggle: toggleFoodSaving } = useModal();
  const { isShown: isShownToGoPacking, toggle: toggleToGoPacking } = useModal();
  const { isShown: isShownThriftShops, toggle: toggleThriftShops } = useModal();

  return (
    <div className="setOfPacman-container">

      <div className="pacman-row1">
        <PacMan imageSource={pacman} imageAlt="Pacman" text="Bulk Shops" handleClick={toggleBulkShops} />
        <Modal
          isShown={isShownBulkShops}
          closeModal={toggleBulkShops}
          headerText={<TextComponent class={possibleClassNames_TextComponent.heading} text={Text.Modal_Bulkshops.modalBulkshop_heading} />}
          imageSource={modalBulkShopPicture}
          imageAlt="Bulk Shop Pictures"
          modalContent=
          {
            <div className="modal-content">
              <TextComponent text={Text.Modal_Bulkshops.modalBulkshop_content} class={possibleClassNames_TextComponent.paragraph} />
            </div>
          }
        />
        <PacMan imageSource={pacman} imageAlt="Pacman" text="Food Saving" handleClick={toggleFoodSaving} />
        <Modal
          isShown={isShownFoodSaving}
          closeModal={toggleFoodSaving}
          headerText={<TextComponent class={possibleClassNames_TextComponent.heading} text={Text.Modal_Foodsaving.modalFoodsaving_heading} />}
          imageSource={modalFoodSavingPicture}
          imageAlt="Food Saving Pictures"
          modalContent=
          {
            <div className="modal-content">
              <TextComponent text={Text.Modal_Foodsaving.modalFoodsaving_content} class={possibleClassNames_TextComponent.paragraph} />
            </div>
          }
        />
      </div>

      <div className="pacman-row2">
        <PacMan imageSource={pacman} imageAlt="Pacman" text="ToGo Boxes & Cups" handleClick={toggleToGoPacking} />
        <Modal
          isShown={isShownToGoPacking}
          closeModal={toggleToGoPacking}
          headerText={<TextComponent text={Text.Modal_Togo.modalTogo_heading} class={possibleClassNames_TextComponent.heading} />}
          imageSource={modalToGoPackingPicture}
          imageAlt="To-go-packing"
          modalContent=
          {
            <div className="modal-content">
              <TextComponent text={Text.Modal_Togo.modalTogo_content} class={possibleClassNames_TextComponent.paragraph} />
            </div>
          }
        />
        <PacMan imageSource={pacman} imageAlt="Pacman" text="Thrift Shops" handleClick={toggleThriftShops} />
        <Modal
          isShown={isShownThriftShops}
          closeModal={toggleThriftShops}
          headerText={<TextComponent text={Text.Modal_Thriftshops.modalThriftshops_heading} class={possibleClassNames_TextComponent.heading} />}
          imageSource={modalThriftShopPicture}
          imageAlt="Thrift Shop Pictures"
          modalContent=
          {
            <div className="modal-content">
            <TextComponent text={Text.Modal_Thriftshops.modalThriftshops_content} class={possibleClassNames_TextComponent.paragraph} />
          </div>
          }
        />
      </div>

    </div>
  );
}
