import React from 'react';
//Hooks
import { useModal } from '../Hooks/useModal';
//Components
import Modal from './Modal';
import TextComponent, { possibleClassNames_TextComponent } from './TextComponent';
//styles
import './Styles/TextComponent.css'
//Image
import LegalNoticePicture from '../Images/modal-legalnotice.png';
//Text
import Text from '../Texts/text.json';

export default function ModalLegalNotice() {
    const { isShown: isShownLegalNotice, toggle: toggleLegalNotice } = useModal();

    return (
        <div><span onClick={toggleLegalNotice}>Legal Notice</span>
            <Modal
                imageAlt="Legal Notice Picture"
                imageSource={LegalNoticePicture}
                headerText={<TextComponent text={Text.Modal_Legal.modalLegal_heading} class={possibleClassNames_TextComponent.heading} />}
                isShown={isShownLegalNotice}
                closeModal={toggleLegalNotice}
                modalContent=
                {
                    <div className="modal-content" >
                        <TextComponent text={Text.Modal_Legal.modalLegal_content} class={possibleClassNames_TextComponent.paragraph} />
                    </div >}
            />
        </div>
    )
}
