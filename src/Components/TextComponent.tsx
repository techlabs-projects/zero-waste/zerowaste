/*
Issue #4: Text Component
•	full width
•	centered text
•	fixed margin left and right
•	text can be given via parameter
*/

import React from 'react';
import ReactMarkdown from 'react-markdown';
//CSS File
import './Styles/TextComponent.css';

export enum possibleClassNames_TextComponent {
  //Heading 
  heading = "heading",
  //paragraph
  paragraph = "p", 
  imageAndText_title = "imageAndText-Title",
  imageAndText_text = "imageAndText-Text"
}

interface textComponentProperties {
  class: possibleClassNames_TextComponent;
  text: string;
}

export default class TextComponent extends React.Component<textComponentProperties, {}> {

  render() {
    return(
      <div className={this.props.class} >
        <ReactMarkdown allowDangerousHtml={true}>{this.props.text}</ReactMarkdown>
      </div>
    )
  }
}