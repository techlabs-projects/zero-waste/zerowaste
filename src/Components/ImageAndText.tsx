import React from "react";
//Components
import './TextComponent'
import TextComponent, { possibleClassNames_TextComponent }  from "./TextComponent";
//CSS File
import "./Styles/ImageAndText.css";

interface imageAndText_Properties {
  title: string;
  text: string;
  buttonText: string;
  imageSource: string;
  imageAlt: string;
}

export default class ImageAndText extends React.Component<imageAndText_Properties,{}> {

  render() {
    return (
      <div className="imageAndText">
        <div className="img">
          <img src={`.${this.props.imageSource}`} alt={this.props.imageAlt} />
        </div>
        <div className="text">
          <TextComponent text={this.props.title} class={possibleClassNames_TextComponent.imageAndText_title}/>
          <TextComponent text={this.props.text} class={possibleClassNames_TextComponent.imageAndText_text}/>
          
          <div id="container">
            <button className="learn-more">
              <span className="circle" aria-hidden="true">
                <span className="icon arrow"></span>
              </span>
              <span className="button-text">{this.props.buttonText}</span>
            </button>
          </div>
        </div>
      </div>
    );
  }
}
