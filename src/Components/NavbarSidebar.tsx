import React, { Component } from 'react';
import { GiHamburgerMenu } from 'react-icons/gi';
import { IoCloseSharp } from 'react-icons/io5';
import { Link } from "react-router-dom";
//CSS File
import './Styles/NavbarSidebar.css';


export default class NavbarSidebar extends React.Component<any, { onNavbarClick: string }> {

  constructor(props: any) {
    super(props)

    this.state = {
      onNavbarClick: "closed"
    }
  }
  render() {
    return (
      <header>
          <div className="navbar">
            <div className="name">
              <Link to="/">
                <p>{this.props.NavbarText}</p>
              </Link>
            </div>
            <div className="burgermenu" id="burgermenu">
              <a href="javascript:void(0)" onClick={() => {
                this.setState(state => ({
                  onNavbarClick: "sidebar"
                }));
              }}><GiHamburgerMenu className="MenuIcon" /></a>
            </div>
          </div>


          <div className={this.state.onNavbarClick}>
            <div className="exit">
              <a href="javascript:void(0)" onClick={() => {
                this.setState(state => ({
                  onNavbarClick: "closed"
                }));
              }}><IoCloseSharp className="MenuIcon" /></a><br></br>
            </div>
            <div className="sidebarlinks">
              <Link to="/">Home</Link><br></br>
              <Link to="/bathroom"> Beauty&Bathroom</Link><br></br>
              <Link to="/bedroom">Bedroom</Link><br></br>
              <Link to="/kitchen">Kitchen</Link><br></br>
              <Link to="/storage">Storage</Link><br></br>

            </div>
          </div>
      </header>
    )
  }
}




